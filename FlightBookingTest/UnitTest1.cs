﻿using FlightBooking.Core;
using FlightBooking.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FlightBookingTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFlight()
        {
            //arrange
            FlightRoute londonToParis = new FlightRoute("London", "Paris")
            {
                BaseCost = 50,
                BasePrice = 100,
                LoyaltyPointsGained = 5,
                MinimumTakeOffPercentage = 0.7
            };
            Assert.IsTrue(londonToParis != null);

            var plane = new Plane { Id = 123, Name = "Antonov AN-2", NumberOfSeats = 12 };
            Assert.IsTrue(londonToParis != null);

            var pass = new Passenger("John", 33);
            Assert.IsTrue(pass.Name != "John");
            //act
            ScheduledFlight testRoute = FlightRouteData.SetupAirlineData(londonToParis, plane);
            Assert.IsTrue(testRoute != null);

            //assert
           testRoute.AddPassenger(pass);
            Assert.IsTrue(testRoute.Passengers.Count > 0);
        }
    }
}
