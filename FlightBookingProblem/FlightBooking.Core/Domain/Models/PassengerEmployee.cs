﻿using System;

namespace FlightBooking.Core
{
    public class PassengerEmployee : Passenger
    {

        public PassengerEmployee()
        {
            this.Type = "Employee";
            this.AllowedBags = 1;
        }

        public PassengerEmployee(params object[] args)
        {
            this.Name = (string)args[0];
            this.Age = Int32.Parse(args[1].ToString());
            this.Type = "Employee";
            this.AllowedBags = 1;
        }
    }
}
