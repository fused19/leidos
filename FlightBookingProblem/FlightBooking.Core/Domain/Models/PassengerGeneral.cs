﻿using System;
using System.Collections.Generic;

namespace FlightBooking.Core
{
    public class PassengerGeneral: Passenger
    {
        public PassengerGeneral()
        {
            this.Type = "General";
            this.AllowedBags = 1;
        }

        public PassengerGeneral(params object[] args)
        {
            this.Name = (string)args[0];
            this.Age = Int32.Parse(args[1].ToString());
            this.Type = "General";
            this.AllowedBags = 1;
        }
    }
}
