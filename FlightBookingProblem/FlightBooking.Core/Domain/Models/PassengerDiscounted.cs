﻿using System;

namespace FlightBooking.Core
{
    public class PassengerDiscounted : Passenger
    {

        public PassengerDiscounted()
        {
            this.Type = "Discounted";
            this.AllowedBags = 0;
        }

        public PassengerDiscounted(params object[] args)
        {
            this.Name = (string)args[0];
            this.Age = Int32.Parse(args[1].ToString());
            this.Type = "Discounted";
            this.AllowedBags = 0;
        }
    }
}
