﻿using System;
using System.Collections.Generic;

namespace FlightBooking.Core
{
    public class PassengerLoyalty: Passenger
    {
        public int LoyaltyPoints { get; set; }
        public bool IsUsingLoyaltyPoints { get; set; }

        public PassengerLoyalty()
        {
            this.Type = "Loyalty";
            this.AllowedBags = 2;
        }

        public PassengerLoyalty(params object[] args)
        {
            this.Name = (string)args[0];
            this.Age = Int32.Parse(args[1].ToString());
            this.LoyaltyPoints = Int32.Parse(args[2].ToString());
            this.IsUsingLoyaltyPoints = bool.Parse(args[3].ToString());
            this.Type = "Loyalty";
            this.AllowedBags = 2;
        }
    }
}
