﻿using System.Collections.Generic;

namespace FlightBooking.Core
{
    public class Passenger
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int AllowedBags { get; set; }

        public string Type = "";

        public Passenger()
        {
            this.AllowedBags = 1;
        }

        public Passenger(string name, int age)
        {
            this.Name = Name;
            this.Age = Age;
        }
    }
}
