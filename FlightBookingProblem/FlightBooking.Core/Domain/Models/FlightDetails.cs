﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightBooking.Core
{
    public class FlightDetails
    {
        public double CostOfFlight { get; set; }
        public double ProfitFromFlight { get; set; }
        public int TotalLoyaltyPointsAccrued { get; set; }
        public int TotalLoyaltyPointsRedeemed { get; set; }
        public int TotalExpectedBaggage { get; set; }
        public int SeatsTaken { get; set; }
        public double ProfitSurplus { get; set; }
        public int Seats { get; set; }
        public double TakeOffPercentage { get; set; }
        public int EmployeesOnBoard { get; set; }
    }
}
