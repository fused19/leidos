﻿using System.Text;

namespace FlightBooking.Core
{
    public class BusinessRulesRelaxed : BusinessRules
    {
        public new StringBuilder Rules(StringBuilder result, FlightDetails flight)
        {
            if (flight.EmployeesOnBoard > flight.TakeOffPercentage &&
                flight.CostOfFlight < flight.ProfitFromFlight
                )
                result.Append("THIS FLIGHT MAY PROCEED");
            else
                result.Append("FLIGHT MAY NOT PROCEED");

            return result;
        }
    }
}