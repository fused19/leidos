﻿using System.Text;

namespace FlightBooking.Core
{
    public class BusinessRules
    {
        public static StringBuilder Rules(StringBuilder result, FlightDetails flight)
        {
            if (flight.ProfitSurplus > 0 &&
                flight.SeatsTaken < flight.Seats &&
                flight.SeatsTaken / (double) flight.Seats > flight.TakeOffPercentage)
                result.Append("THIS FLIGHT MAY PROCEED");
            else
                result.Append("FLIGHT MAY NOT PROCEED");

            return result;
        }
    }
}