﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace FlightBooking.Core
{
    public class ScheduledFlight
    {
        private readonly string VERTICAL_WHITE_SPACE = Environment.NewLine + Environment.NewLine;
        private readonly string NEW_LINE = Environment.NewLine;
        private const string INDENTATION = "    ";

        private FlightRoute FlightRoute { get; set; }
        
        private Plane Aircraft { get; set; }
        
        public List<Passenger> Passengers { get; set; }

        public ScheduledFlight(FlightRoute flightRoute)
        {
            FlightRoute = flightRoute;
            Passengers = new List<Passenger>();
        }

        public void AddPassenger(Passenger passenger)
        {
            Passengers.Add(passenger);
        }

        public void SetAircraftForRoute(Plane aircraft)
        {
            Aircraft = aircraft;
        }

        public string GetSummary()
        {
            FlightDetails flight = new FlightDetails();
            flight.CostOfFlight = CostofFlight(Passengers.Count(), FlightRoute.BaseCost);
            flight.EmployeesOnBoard = Passengers.Count(c => c.Type == "Employee");
            flight.TotalExpectedBaggage  = Passengers.Sum(c => c.AllowedBags);

            StringBuilder output = new StringBuilder("Flight summary for " + FlightRoute.Title);
            
            PassengerDetails(output, flight);

            PassengerText(output, flight);
 
            RevenueText(output, flight);

            LoyaltyText(output, flight);

            BusinessRules.Rules(output, flight);

            return output.ToString();
        }

        private static double CostofFlight(int count, double cost)
        {
            return count * cost;
        }

        private StringBuilder PassengerText(StringBuilder result, FlightDetails flight)
        {
            result.Append(VERTICAL_WHITE_SPACE);

            result.Append("Total passengers: " + Passengers.Count());
            result.Append(NEW_LINE);
            result.Append(INDENTATION + "General sales: " + Passengers.Count(p => p.GetType() == typeof(PassengerGeneral)));
            result.Append(NEW_LINE);
            result.Append(INDENTATION + "Loyalty member sales: " +
                      Passengers.Count(p => p.GetType() == typeof(PassengerLoyalty)));
            result.Append(NEW_LINE);
            result.Append("Airline employee comps: " +
                      Passengers.Count(p => p.GetType() == typeof(PassengerEmployee)));

            result.Append(VERTICAL_WHITE_SPACE);
            result.Append("Total expected baggage: " +flight.TotalExpectedBaggage);

            return result;
        }

        private StringBuilder RevenueText(StringBuilder result, FlightDetails flight)
        {
            result.Append(VERTICAL_WHITE_SPACE);

            result.Append("Total revenue from flight: " + flight.ProfitFromFlight);
            result.Append(NEW_LINE);
            result.Append("Total costs from flight: " + flight.CostOfFlight);
            result.Append(NEW_LINE);

            var profitSurplus = flight.ProfitFromFlight - flight.CostOfFlight;

            result.Append((profitSurplus > 0 ? "Flight generating profit of: " : "Flight losing money of: ") + profitSurplus);

            return result;
        }

        private StringBuilder LoyaltyText(StringBuilder result, FlightDetails flight)
        {
            result.Append(VERTICAL_WHITE_SPACE);

            result.Append("Total loyalty points given away: " + flight.TotalLoyaltyPointsAccrued + NEW_LINE);
            result.Append("Total loyalty points redeemed: " + flight.TotalLoyaltyPointsRedeemed + NEW_LINE);

            result.Append(VERTICAL_WHITE_SPACE);
            return result;
        }

        private void PassengerDetails(StringBuilder result, FlightDetails flight)
        {
            foreach (var passenger in Passengers)
            {
                switch (passenger.Type)
                {
                    case("General"):
                    {
                        flight.ProfitFromFlight += FlightRoute.BasePrice;
                        break;
                    }
                    case("Loyalty"):
                    {
                        if (passenger is PassengerLoyalty person && person.IsUsingLoyaltyPoints)
                        {
                            int loyaltyPointsRedeemed = Convert.ToInt32(Math.Ceiling(FlightRoute.BasePrice));
                            person.LoyaltyPoints -= loyaltyPointsRedeemed;
                            flight.TotalLoyaltyPointsRedeemed += loyaltyPointsRedeemed;
                        }
                        else
                        {
                            flight.TotalLoyaltyPointsAccrued += FlightRoute.LoyaltyPointsGained;
                            flight.ProfitFromFlight += FlightRoute.BasePrice;
                        }
                        break;
                    }
                    case("Employee"):
                    {
                        break;
                    }
                    case("Discounted"):
                    {
                        flight.ProfitFromFlight += (FlightRoute.BasePrice / 2);
                        break;
                    }
                }
            }
        }
    }
}