﻿
namespace FlightBooking.Core.Domain
{
     public  class FlightRouteData
    {
        public static ScheduledFlight _scheduledFlight;

        public static ScheduledFlight SetupAirlineData()
        {
            FlightRoute londonToParis = new FlightRoute("London", "Paris")
            {
                BaseCost = 50,
                BasePrice = 100,
                LoyaltyPointsGained = 5,
                MinimumTakeOffPercentage = 0.7
            };

            _scheduledFlight = new ScheduledFlight(londonToParis);

            _scheduledFlight.SetAircraftForRoute(
                new Plane { Id = 123, Name = "Antonov AN-2", NumberOfSeats = 12 });
            return _scheduledFlight;
        }

        public static ScheduledFlight SetupAirlineData(FlightRoute route, Plane aircraft)
        {
            _scheduledFlight = new ScheduledFlight(route);
            _scheduledFlight.SetAircraftForRoute(aircraft);
            return _scheduledFlight;
        }
    }
}
