﻿using System;
using System.Linq;

namespace FlightBooking.Core.Domain
{
    public class PassengerProcessing
    {
        public static T GetObject<T>(params object[] args)
        {
            var ctor = typeof(T).GetConstructors().First(c => c.GetParameters().Length == 1);
            var classInstance = (T)Activator.CreateInstance(typeof(T), new[] { args });
            var instance = (T)ctor.Invoke(classInstance, new[] { args });
            return classInstance;
        }
    }
}
