﻿using System;
using System.Linq;
using FlightBooking.Core;
using FlightBooking.Core.Domain;

namespace FlightBookingProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            var _scheduledFlight = FlightRouteData.SetupAirlineData();
            string command = "";
            do
            {
                command = Console.ReadLine() ?? "";
                var enteredText = command.ToLower();
                string[] passengerSegments = enteredText.Split(' ').Skip(2).ToArray();
                if (enteredText.Contains("print summary"))
                {
                    Console.WriteLine();
                    Console.WriteLine(_scheduledFlight.GetSummary());
                }
                else if (enteredText.Contains("add general"))
                {
                    PassengerGeneral pass = PassengerProcessing.GetObject<PassengerGeneral>(passengerSegments);
                    _scheduledFlight.AddPassenger(pass);
                }
                else if (enteredText.Contains("add loyalty"))
                {
                    PassengerLoyalty pass = PassengerProcessing.GetObject<PassengerLoyalty>(passengerSegments);
                    _scheduledFlight.AddPassenger(pass);
                }
                else if (enteredText.Contains("add airline"))
                {
                    PassengerEmployee pass = PassengerProcessing.GetObject<PassengerEmployee>(passengerSegments);
                    _scheduledFlight.AddPassenger(pass);
                }
                else if (enteredText.Contains("add discounted"))
                {
                    PassengerDiscounted pass = PassengerProcessing.GetObject<PassengerDiscounted>(passengerSegments);
                    _scheduledFlight.AddPassenger(pass);
                }
                else if (enteredText.Contains("exit"))
                {
                    Environment.Exit(1);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("UNKNOWN INPUT");
                    Console.ResetColor();
                }
            } while (command != "exit");
        }
    }
}